package com.zopa.controller;

import com.zopa.controller.api.InputValidation;
import com.zopa.controller.api.LenderOffersLoader;
import com.zopa.controller.api.QuoteCalculator;
import com.zopa.controller.api.QuoteOutputFormatter;
import com.zopa.service.lender.CSVLenderSortedOffersLoader;
import com.zopa.service.quote.CompoundingMonthlyInterestQuoteGenerator;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;
import com.zopa.service.validation.InputValidator;
import com.zopa.service.validation.exceptions.RequestedValueIsNotIncremental;
import com.zopa.service.validation.exceptions.RequestedValueNotInAllowedRange;
import com.zopa.view.BasicQuoteOutputFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;

import static com.zopa.model.constans.DataRestrictionConstans.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class QuoteReportControllerTest {

    private static final String TEST_FILE = "src/test/java/com/zopa/service/resources/marketDataZopa.csv";

    private QuoteReportController quoteReportController;

    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private final PrintStream printStreamOutput = System.out;

    @AfterEach
    public void restoreStream() {
        System.setOut(printStreamOutput);
    }

    @BeforeEach
    void setUp() {
        QuoteCalculator quoteCalculator = new CompoundingMonthlyInterestQuoteGenerator(NUMBER_OF_MONTHS_REQUESTED);
        LenderOffersLoader lenderOffersLoader = new CSVLenderSortedOffersLoader(TEST_FILE);
        InputValidation inputValidation = new InputValidator(START_RANGE, END_RANGE, INCREMENT);
        QuoteOutputFormatter quoteOutputFormatter = new BasicQuoteOutputFormatter();
        quoteReportController = new QuoteReportController(quoteCalculator, lenderOffersLoader, inputValidation, quoteOutputFormatter);

        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @Test
    void shouldCalculateAndPrintReport() {
        quoteReportController.getQuoteReportForRequestedValue(new BigDecimal(1000));

        String expectedMessage = "Requested amount: £1000,00\n" +
                "Rate: 7,0%\n" +
                "Monthly repayment: £30,88\n" +
                "Total repayment: £1111,68\n";

        assertEquals(expectedMessage, byteArrayOutputStream.toString());

    }

    @Test
    void shouldPrintErrorReportAboutNotIncrementalValue() {

        quoteReportController.getQuoteReportForRequestedValue(new BigDecimal(1008));

        String expectedMessage = new RequestedValueIsNotIncremental(INCREMENT).getMessage();

        assertEquals(expectedMessage.trim(), byteArrayOutputStream.toString().trim());

    }

    @Test
    void shouldPrintErrorReportAboutValueNotInRangeAllowed() {

        quoteReportController.getQuoteReportForRequestedValue(new BigDecimal(99999999));

        String expectedMessage = new RequestedValueNotInAllowedRange(START_RANGE, END_RANGE).getMessage();

        assertEquals(expectedMessage.trim(), byteArrayOutputStream.toString().trim());

    }

    @Test
    void shouldPrintErrorReportAboutNotEnoughBudgetToCoverLoan() {

        quoteReportController.getQuoteReportForRequestedValue(new BigDecimal(4000));

        String expectedMessage = new NotEnoughBudgetToCoverRequestedValueException().getMessage();

        assertEquals(expectedMessage.trim(), byteArrayOutputStream.toString().trim());

    }
}