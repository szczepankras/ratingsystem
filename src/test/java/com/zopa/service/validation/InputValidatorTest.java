package com.zopa.service.validation;

import com.zopa.controller.api.InputValidation;
import com.zopa.service.validation.exceptions.RequestedValueIsNotIncremental;
import com.zopa.service.validation.exceptions.RequestedValueNotInAllowedRange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class InputValidatorTest {

    private BigDecimal startRange;
    private BigDecimal endRange;
    private BigDecimal incrementValue;
    private InputValidation inputValidator;

    @BeforeEach
    void setUp() {
        startRange = new BigDecimal(1000);
        endRange = new BigDecimal(15000);
        incrementValue = new BigDecimal(100);
        inputValidator = new InputValidator(startRange, endRange, incrementValue);
    }

    @Test
    void isInAllowedRangeTest() throws RequestedValueNotInAllowedRange, RequestedValueIsNotIncremental {
        assertTrue(inputValidator.validate(new BigDecimal(2500)));
    }

    @Test
    void isNotInAllowedRangeTest() {
        assertThrows(RequestedValueNotInAllowedRange.class, () -> inputValidator.validate(new BigDecimal(2500000)));

        Throwable exception = assertThrows(RequestedValueNotInAllowedRange.class, () -> {
            throw new RequestedValueNotInAllowedRange(startRange, endRange);
        });
        assertEquals("Requested value is not in allowed range (1000 - 15000)", exception.getMessage());
    }

    @Test
    void isInInclusiveLeftSiteAllowedRangeTest() throws RequestedValueNotInAllowedRange, RequestedValueIsNotIncremental {
        assertTrue(inputValidator.validate(new BigDecimal(1000)));
    }

    @Test
    void isInInclusiveRightSiteAllowedRangeTest() throws RequestedValueNotInAllowedRange, RequestedValueIsNotIncremental {
        assertTrue(inputValidator.validate(new BigDecimal(15000)));
    }

    @Test
    void isIncrementOfTest() throws RequestedValueNotInAllowedRange, RequestedValueIsNotIncremental {
        assertTrue(inputValidator.validate(new BigDecimal(1000)));
    }

    @Test
    void isNotIncrementOfTest() {
        assertThrows(RequestedValueIsNotIncremental.class, () -> inputValidator.validate(new BigDecimal(2019)));

        Throwable exception = assertThrows(RequestedValueIsNotIncremental.class, () -> {
            throw new RequestedValueIsNotIncremental(incrementValue);
        });
        assertEquals("Requested value should be increment of 100", exception.getMessage());
    }
}