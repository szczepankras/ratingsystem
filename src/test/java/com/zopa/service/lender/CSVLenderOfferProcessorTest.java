package com.zopa.service.lender;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.lender.utils.FileLoader;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CSVLenderOfferProcessorTest {

    private static final String TEST_FILE = "src/test/java/com/zopa/service/resources/marketDataTest.csv";

    @Test
    void buildOffersListTest() {
        CSVLenderOfferProcessor csvLenderOfferProcessor = new CSVLenderOfferProcessor();
        Stream testedStream = Stream.empty();
        try {
            testedStream = FileLoader.loadFileFromPath(Paths.get(TEST_FILE));
        } catch (LoadOffersException e) {
            e.printStackTrace();
        }

        List<LenderOffer> lenderOffers = csvLenderOfferProcessor.buildOffersList(testedStream);

        List<LenderOffer> expectedLenderOfferList = new LinkedList<>();
        expectedLenderOfferList.add(new LenderOffer("Szczepan", 0.48, new BigDecimal(1020)));
        expectedLenderOfferList.add(new LenderOffer("John", 0.078, new BigDecimal(100)));

        assertEquals(expectedLenderOfferList, lenderOffers);
    }
}