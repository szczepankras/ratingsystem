package com.zopa.service.lender.utils;

import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static com.zopa.service.lender.utils.FileLoader.loadFileFromPath;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FileLoaderTest {

    private static final String FILE_PATH = "src/main/resources/marketData.csv";
    private Path path;

    @BeforeEach
    void setUp() {
        path = Paths.get(FILE_PATH);
    }

    @Test
    void loadFileFromPathTest() {
        Stream outputStream = null;
        try {
            outputStream = loadFileFromPath(path);
        } catch (LoadOffersException e) {
            e.printStackTrace();
        }

        assertNotNull(outputStream);
    }

    @Test
    void shouldThrowLoadResourceExceptionForNotExistingFile() {
        assertThrows(LoadOffersException.class, () -> loadFileFromPath(Paths.get("fake.file")));
    }
}