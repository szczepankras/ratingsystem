package com.zopa.service.lender;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVLenderSortedOffersLoaderTest {

    private static final String TEST_FILE = "src/test/java/com/zopa/service/resources/marketDataTest.csv";

    @Test
    void loadSortedOffersFromFileTest() throws LoadOffersException {
        CSVLenderSortedOffersLoader lenderSortedOffersLoader = new CSVLenderSortedOffersLoader(TEST_FILE);

        List<LenderOffer> lenderOfferList = lenderSortedOffersLoader.loadOffers();

        List<LenderOffer> expectedLenderOfferList = new LinkedList<>();

        expectedLenderOfferList.add(new LenderOffer("John", 0.078, new BigDecimal(100)));
        expectedLenderOfferList.add(new LenderOffer("Szczepan", 0.48, new BigDecimal(1020)));

        assertEquals(expectedLenderOfferList, lenderOfferList);
    }
}