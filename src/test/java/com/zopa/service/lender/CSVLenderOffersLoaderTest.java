package com.zopa.service.lender;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVLenderOffersLoaderTest {

    private static final String TEST_FILE = "src/test/java/com/zopa/service/resources/marketDataTest.csv";
    private static final String EMPTY_TEST_FILE = "src/test/java/com/zopa/service/resources/marketDataTestEmpty.csv";

    @Test
    void shouldLoadOffersFromFileTest() throws LoadOffersException {
        CSVLenderOffersLoader CSVLenderOffersLoader = new CSVLenderOffersLoader(TEST_FILE);

        List<LenderOffer> lenderOffersLoaded = CSVLenderOffersLoader.loadOffers();

        List<LenderOffer> expectedLenderOfferList = new LinkedList<>();
        expectedLenderOfferList.add(new LenderOffer("Szczepan", 0.48, new BigDecimal(1020)));
        expectedLenderOfferList.add(new LenderOffer("John", 0.078, new BigDecimal(100)));

        assertEquals(expectedLenderOfferList, lenderOffersLoaded);
    }

    @Test
    void shouldLoadEmptyOffersListWhenFileIsEmptyTest() throws LoadOffersException {
        CSVLenderOffersLoader CSVLenderOffersLoader = new CSVLenderOffersLoader(EMPTY_TEST_FILE);

        List<LenderOffer> lenderOffersLoaded = CSVLenderOffersLoader.loadOffers();

        assertEquals(0, lenderOffersLoaded.size());
    }

    @Test
    void shouldThrowsExceptionWhenFileDoesNotExists() {
        CSVLenderOffersLoader CSVLenderOffersLoader = new CSVLenderOffersLoader("Fake");

        assertThrows(LoadOffersException.class, CSVLenderOffersLoader::loadOffers);
    }

}