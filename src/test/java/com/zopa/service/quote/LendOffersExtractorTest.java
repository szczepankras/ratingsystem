package com.zopa.service.quote;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LendOffersExtractorTest {

    List<LenderOffer> givenLenderOffers;

    @BeforeEach
    void setup() {
        givenLenderOffers = new LinkedList<>();
        givenLenderOffers.add(new LenderOffer("John", 0.078, new BigDecimal(100)));
        givenLenderOffers.add(new LenderOffer("Szczepan", 0.48, new BigDecimal(1020)));
    }

    @Test
    void extractOffersToCoverGivenExactAmountTest() throws NotEnoughBudgetToCoverRequestedValueException {

        List<LenderOffer> lenderOfferList = LendOffersExtractor.extractLenderOffersForGivenAmount(givenLenderOffers, new BigDecimal(100));

        List<LenderOffer> expectedLenderOffers = new LinkedList<>();
        expectedLenderOffers.add(new LenderOffer("John", 0.078, new BigDecimal(100)));

        assertEquals(expectedLenderOffers, lenderOfferList);
    }

    @Test
    void extractOffersToCoverGivenNotExactAmountTest() throws NotEnoughBudgetToCoverRequestedValueException {

        List<LenderOffer> lenderOfferList = LendOffersExtractor.extractLenderOffersForGivenAmount(givenLenderOffers, new BigDecimal(110));

        List<LenderOffer> expectedLenderOffers = new LinkedList<>();
        expectedLenderOffers.add(new LenderOffer("John", 0.078, new BigDecimal(100)));
        expectedLenderOffers.add(new LenderOffer("Szczepan", 0.48, new BigDecimal(10)));

        assertEquals(expectedLenderOffers, lenderOfferList);
    }

    @Test
    void shouldThrowsExceptionWhenEnoughOffersDoesNotExistForGivenAmountTest() {

        assertThrows(NotEnoughBudgetToCoverRequestedValueException.class, ()->LendOffersExtractor.extractLenderOffersForGivenAmount(givenLenderOffers, new BigDecimal(10000)));

    }
}