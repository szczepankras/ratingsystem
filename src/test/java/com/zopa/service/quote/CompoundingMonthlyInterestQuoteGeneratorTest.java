package com.zopa.service.quote;

import com.zopa.controller.api.QuoteCalculator;
import com.zopa.model.lender.LenderOffer;
import com.zopa.model.quote.Quote;
import com.zopa.service.lender.CSVLenderSortedOffersLoader;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.zopa.model.constans.DataRestrictionConstans.NUMBER_OF_MONTHS_REQUESTED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CompoundingMonthlyInterestQuoteGeneratorTest {

    private static final String pathToFile = "src/test/java/com/zopa/service/resources/marketDataZopa.csv";

    private List<LenderOffer> lenderOffers;
    private QuoteCalculator quoteCalculator;

    @BeforeEach
    void setUp() throws LoadOffersException {
        CSVLenderSortedOffersLoader csvLenderSortedOffersLoader = new CSVLenderSortedOffersLoader(pathToFile);
        lenderOffers = csvLenderSortedOffersLoader.loadOffers();
        quoteCalculator = new CompoundingMonthlyInterestQuoteGenerator(NUMBER_OF_MONTHS_REQUESTED);
    }

    @Test
    void generateQuoteTest() throws NotEnoughBudgetToCoverRequestedValueException {

        Quote quote = quoteCalculator.calculate(lenderOffers, new BigDecimal(1000));

        Quote expectedQuote = new Quote(new BigDecimal(1000),
                new BigDecimal(30.88).setScale(2, RoundingMode.HALF_UP),
                new BigDecimal(1111.68).setScale(2, RoundingMode.HALF_UP),
                7.0);

        assertEquals(expectedQuote, quote);
    }

    @Test
    void shouldThrowExceptionWithNotEnoughBudgetGeneratingQuoteTest() {

        assertThrows(NotEnoughBudgetToCoverRequestedValueException.class, () -> quoteCalculator.calculate(lenderOffers, new BigDecimal(100000)));
    }
}