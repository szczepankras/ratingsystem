package com.zopa.service.quote.utils;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CompoundingMonthlyInterestCalculatorTest {

    @Test
    void compoundMonthlyInterestTest() {

        BigDecimal principal = new BigDecimal(1000);
        double annualInterestRate = 0.07;
        int timesOfInterestPerYear = 12;
        int numberOfYears = 3;

        CompoundingMonthlyInterestCalculator compoundingMonthlyInterestCalculator =
                new CompoundingMonthlyInterestCalculator(principal, annualInterestRate, timesOfInterestPerYear, numberOfYears);

        BigDecimal compoundedValue = compoundingMonthlyInterestCalculator.compoundMonthlyInterest();

        assertEquals(new BigDecimal(String.valueOf(30.88)), compoundedValue);
    }

}