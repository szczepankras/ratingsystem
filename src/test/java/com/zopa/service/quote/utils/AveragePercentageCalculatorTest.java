package com.zopa.service.quote.utils;

import com.zopa.model.lender.LenderOffer;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static com.zopa.service.quote.utils.AveragePercentageCalculator.averageRate;
import static org.junit.jupiter.api.Assertions.*;

class AveragePercentageCalculatorTest {

    @Test
    void averagePercentageRateTest(){
        List<LenderOffer> givenLenderOffers = new LinkedList<>();

        givenLenderOffers.add(new LenderOffer("John", 0.078, new BigDecimal(100)));
        givenLenderOffers.add(new LenderOffer("Szczepan", 0.48, new BigDecimal(1020)));

        double calculatedValue = averageRate(givenLenderOffers);
        double expectedValue = 0.279;
        assertEquals(expectedValue, calculatedValue);
    }

}