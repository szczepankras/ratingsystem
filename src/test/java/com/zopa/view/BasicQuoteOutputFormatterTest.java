package com.zopa.view;

import com.zopa.model.quote.Quote;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BasicQuoteOutputFormatterTest {

    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private final PrintStream printStreamOutput = System.out;

    @BeforeEach
    public void setUpStream() {
        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @AfterEach
    public void restoreStream() {
        System.setOut(printStreamOutput);
    }

    @Test
    void printQuoteReportTest() {

        BasicQuoteOutputFormatter basicQuoteOutputFormatter = new BasicQuoteOutputFormatter();
        Quote quote = new Quote(new BigDecimal(10), new BigDecimal(12), new BigDecimal(6), 7.0);
        basicQuoteOutputFormatter.printQuoteReport(quote);

        String expectedMessage = "Requested amount: £10,00\n" +
                "Rate: 7,0%\n" +
                "Monthly repayment: £12,00\n" +
                "Total repayment: £6,00\n";

        assertEquals(expectedMessage, byteArrayOutputStream.toString());

    }

    @Test
    void printErrorReportTest() {

        BasicQuoteOutputFormatter basicQuoteOutputFormatter = new BasicQuoteOutputFormatter();

        String error = "Error 1";

        basicQuoteOutputFormatter.printErrorReport(error);

        String expectedMessage = "Error 1\n";

        assertEquals(expectedMessage, byteArrayOutputStream.toString());

    }
}