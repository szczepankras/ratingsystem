package com.zopa.model.lender;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class LenderOffer implements Comparable {

    private String lender;
    private double rate;
    private BigDecimal availableCash;

    @Override
    public int compareTo(Object obj) {
        LenderOffer lenderOffer = (LenderOffer)obj;
        return Double.compare(rate, lenderOffer.getRate());
    }
}
