package com.zopa.model.constans;

import java.math.BigDecimal;

public class DataRestrictionConstans {

    public static final BigDecimal START_RANGE = new BigDecimal(1000);
    public static final BigDecimal END_RANGE = new BigDecimal(15000);
    public static final BigDecimal INCREMENT = new BigDecimal(100);
    public static final int NUMBER_OF_MONTHS_REQUESTED = 36;
}
