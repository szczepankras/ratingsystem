package com.zopa.model.quote;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Quote {

    private BigDecimal loanAmount;
    private BigDecimal monthlyRepayment;
    private BigDecimal totalRepayment;
    private double rate;

}
