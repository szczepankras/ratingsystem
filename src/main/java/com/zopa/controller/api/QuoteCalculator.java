package com.zopa.controller.api;

import com.zopa.model.lender.LenderOffer;
import com.zopa.model.quote.Quote;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;

import java.math.BigDecimal;
import java.util.Collection;

public interface QuoteCalculator {

    Quote calculate(Collection<LenderOffer> lenderOffers, BigDecimal requestedAmount) throws NotEnoughBudgetToCoverRequestedValueException;
}
