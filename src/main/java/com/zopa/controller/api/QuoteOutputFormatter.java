package com.zopa.controller.api;

import com.zopa.model.quote.Quote;

public interface QuoteOutputFormatter {

    void printQuoteReport(Quote quote);

    void printErrorReport(String error);
}
