package com.zopa.controller.api;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;

import java.util.List;

public interface LenderOffersLoader {

    List<LenderOffer> loadOffers() throws LoadOffersException;
}
