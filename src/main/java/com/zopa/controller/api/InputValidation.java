package com.zopa.controller.api;

import com.zopa.service.validation.exceptions.RequestedValueIsNotIncremental;
import com.zopa.service.validation.exceptions.RequestedValueNotInAllowedRange;

import java.math.BigDecimal;

public interface InputValidation {

    boolean validate(BigDecimal value) throws RequestedValueNotInAllowedRange, RequestedValueIsNotIncremental;
}
