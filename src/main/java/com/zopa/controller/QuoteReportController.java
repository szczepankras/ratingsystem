package com.zopa.controller;

import com.zopa.controller.api.InputValidation;
import com.zopa.controller.api.LenderOffersLoader;
import com.zopa.controller.api.QuoteCalculator;
import com.zopa.controller.api.QuoteOutputFormatter;
import com.zopa.model.lender.LenderOffer;
import com.zopa.model.quote.Quote;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;
import com.zopa.service.validation.exceptions.RequestedValueIsNotIncremental;
import com.zopa.service.validation.exceptions.RequestedValueNotInAllowedRange;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class QuoteReportController {

    @NonNull
    private QuoteCalculator quoteCalculator;
    @NonNull
    private LenderOffersLoader lenderOffersLoader;
    @NonNull
    private InputValidation inputValidation;
    @NonNull
    private QuoteOutputFormatter quoteOutputFormatter;

    public void getQuoteReportForRequestedValue(BigDecimal inputValue) {
        Optional<Quote> quote = Optional.empty();
        if (validateInput(inputValue)) {
            quote = getQuoteForRequestedAmount(inputValue);
        }
        quote.ifPresent(generatedQuote -> quoteOutputFormatter.printQuoteReport(generatedQuote));
    }

    private boolean validateInput(BigDecimal inputValue) {
        boolean result = false;
        try {
            result = inputValidation.validate(inputValue);
        } catch (RequestedValueNotInAllowedRange requestedValueNotInAllowedRange) {
            quoteOutputFormatter.printErrorReport(requestedValueNotInAllowedRange.getMessage());
        } catch (RequestedValueIsNotIncremental requestedValueIsNotIncremental) {
            quoteOutputFormatter.printErrorReport(requestedValueIsNotIncremental.getMessage());
        }
        return result;
    }

    private Optional<Quote> getQuoteForRequestedAmount(BigDecimal requestedAmount) {
        List<LenderOffer> lenderOffers = getLenderOffersFromRepository();
        Optional<Quote> quote = Optional.empty();
        try {
            if (!lenderOffers.isEmpty()) {
                quote = Optional.of(quoteCalculator.calculate(lenderOffers, requestedAmount));
            }
        } catch (NotEnoughBudgetToCoverRequestedValueException e) {
            quoteOutputFormatter.printErrorReport(e.getMessage());
        }
        return quote;
    }

    private List<LenderOffer> getLenderOffersFromRepository() {
        List<LenderOffer> lenderOffers = new LinkedList<>();
        try {
            lenderOffers = lenderOffersLoader.loadOffers();
        } catch (LoadOffersException e) {
            quoteOutputFormatter.printErrorReport(e.getMessage());
        }
        return lenderOffers;
    }
}
