package com.zopa.service.lender;

import com.zopa.model.lender.LenderOffer;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CSVLenderOfferProcessor {

    private static final String COMMA_DELIMITER = ",";
    private static final String HEADER = "Lender,Rate,Available";

    public List<LenderOffer> buildOffersList(Stream stream) {
        @SuppressWarnings("unchecked")
        List<LenderOffer> offersList = (List<LenderOffer>) stream.filter(CSVLenderOfferProcessor::isNotHeaderAndNotEmpty)
                .map(CSVLenderOfferProcessor::buildLenderOffer)
                .collect(Collectors.toList());
        return offersList;
    }

    private static boolean isNotHeaderAndNotEmpty(Object record) {
        String line = (String) record;
        return !line.isEmpty() && !line.trim().equals(HEADER.trim());
    }

    private static LenderOffer buildLenderOffer(Object record) {
        String line = (String) record;
        String[] data = line.trim().split(COMMA_DELIMITER);
        LenderOffer lenderOffer = null;

        if (data.length == 3) {
            String lender = data[0];
            double rate = Double.parseDouble(data[1]);
            BigDecimal availableCash = new BigDecimal(data[2]);
            lenderOffer = new LenderOffer(lender, rate, availableCash);
        }

        return lenderOffer;
    }
}
