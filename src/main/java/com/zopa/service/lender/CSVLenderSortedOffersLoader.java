package com.zopa.service.lender;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;

import java.util.List;
import java.util.stream.Collectors;

public class CSVLenderSortedOffersLoader extends CSVLenderOffersLoader {

    public CSVLenderSortedOffersLoader(String filePath) {
        super(filePath);
    }

    @Override
    public List<LenderOffer> loadOffers() throws LoadOffersException {
        return super.loadOffers().stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
