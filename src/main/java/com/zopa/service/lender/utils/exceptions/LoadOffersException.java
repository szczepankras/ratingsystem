package com.zopa.service.lender.utils.exceptions;


public class LoadOffersException extends Exception {
    public LoadOffersException(String message) {
        super(message);
    }
}
