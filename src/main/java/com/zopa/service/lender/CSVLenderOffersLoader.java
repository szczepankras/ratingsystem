package com.zopa.service.lender;

import com.zopa.controller.api.LenderOffersLoader;
import com.zopa.model.lender.LenderOffer;
import com.zopa.service.lender.utils.exceptions.LoadOffersException;
import lombok.AllArgsConstructor;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.zopa.service.lender.utils.FileLoader.loadFileFromPath;

@AllArgsConstructor
public class CSVLenderOffersLoader extends CSVLenderOfferProcessor implements LenderOffersLoader {

    private String filePath;

    @Override
    public List<LenderOffer> loadOffers() throws LoadOffersException {
        Path path = Paths.get(filePath);
        List<LenderOffer> lenderOffers = buildOffersList(loadFileFromPath(path));
        return lenderOffers;
    }
}
