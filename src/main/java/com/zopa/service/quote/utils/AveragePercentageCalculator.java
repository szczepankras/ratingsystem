package com.zopa.service.quote.utils;

import com.zopa.model.lender.LenderOffer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Optional;

public class AveragePercentageCalculator {

    public static double averageRate(Collection<LenderOffer> lenderOffers) {
        Optional<LenderOffer> lenderOfferRateSum = lenderOffers.stream()
                .reduce(AveragePercentageCalculator::lenderOfferRateReducer);
        double averageRate = 0.0;
        if (lenderOfferRateSum.isPresent()) {
            averageRate = lenderOfferRateSum.get().getRate() / (double) lenderOffers.size();
        }
        BigDecimal bigDecimalForRound = new BigDecimal(Double.toString(averageRate));
        bigDecimalForRound = bigDecimalForRound.setScale(3, RoundingMode.HALF_UP);
        return bigDecimalForRound.doubleValue();
    }

    private static LenderOffer lenderOfferRateReducer(LenderOffer lenderOffer1, LenderOffer lenderOffer2) {
        return new LenderOffer("", lenderOffer1.getRate() + lenderOffer2.getRate(), BigDecimal.ZERO);
    }
}
