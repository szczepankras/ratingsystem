package com.zopa.service.quote.utils;

import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

@AllArgsConstructor
public class CompoundingMonthlyInterestCalculator {

    BigDecimal principal;
    double annualInterestRate;
    int timesOfInterestPerYear;
    int numberOfYears;

    public BigDecimal compoundMonthlyInterest() {

        double interestRate = annualInterestRate / timesOfInterestPerYear;
        int exponent = timesOfInterestPerYear * numberOfYears;

        BigDecimal factorInterestRateDecimal = new BigDecimal(String.valueOf(interestRate));
        factorInterestRateDecimal = factorInterestRateDecimal.add(BigDecimal.ONE);

        BigDecimal costPerMonth = principal.multiply(factorInterestRateDecimal.pow(exponent)).multiply(BigDecimal.ONE.subtract(factorInterestRateDecimal));
        BigDecimal divider = BigDecimal.ONE.subtract(factorInterestRateDecimal.pow(exponent));
        costPerMonth = costPerMonth.divide(divider, 2, RoundingMode.HALF_DOWN);

        return costPerMonth;
    }
}
