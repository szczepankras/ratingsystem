package com.zopa.service.quote;

import com.zopa.model.lender.LenderOffer;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LendOffersExtractor {

    public static List<LenderOffer> extractLenderOffersForGivenAmount(Collection<LenderOffer> lenderOffers, BigDecimal amount)
            throws NotEnoughBudgetToCoverRequestedValueException {
        if (isNotEnoughBudgetInMarket(amount, lenderOffers)) {
            throw new NotEnoughBudgetToCoverRequestedValueException();
        }

        BigDecimal currentSum = BigDecimal.ZERO;

        LinkedList<LenderOffer> extractedLenderOffers = new LinkedList<>();

        for (LenderOffer lenderOffer : lenderOffers) {
            if (currentSum.compareTo(amount) == -1) {
                currentSum = currentSum.add(lenderOffer.getAvailableCash());
                extractedLenderOffers.add(lenderOffer);
            }
        }
        LenderOffer lastLenderOffer = extractedLenderOffers.getLast();
        extractedLenderOffers.set(extractedLenderOffers.size()-1, assignDifferenceForRequestedAmount(lastLenderOffer, amount, currentSum));
        return extractedLenderOffers;
    }

    private static boolean isNotEnoughBudgetInMarket(BigDecimal amount, Collection<LenderOffer> lenderOffers) {
        Optional<LenderOffer> lenderOffersSum = sumAllAvailableCash(lenderOffers);
        if (lenderOffersSum.isPresent()) {
            if (amount.compareTo(lenderOffersSum.get().getAvailableCash()) == 1) {
                return true;
            }
        }
        return false;
    }

    private static Optional<LenderOffer> sumAllAvailableCash(Collection<LenderOffer> lenderOffers) {
        return lenderOffers.stream().reduce(LendOffersExtractor::cashLenderOfferReducer);
    }

    private static LenderOffer cashLenderOfferReducer(LenderOffer lenderOffer1, LenderOffer lenderOffer2) {
        return new LenderOffer("", 0, lenderOffer1.getAvailableCash().add(lenderOffer2.getAvailableCash()));
    }

    private static LenderOffer assignDifferenceForRequestedAmount(LenderOffer lenderOffer, BigDecimal amount, BigDecimal currentSum) {
        BigDecimal differenceInRequestedValue = amount.subtract(currentSum);
        BigDecimal differenceForGivenOffer = lenderOffer.getAvailableCash().subtract(differenceInRequestedValue.abs());
        return new LenderOffer(lenderOffer.getLender(), lenderOffer.getRate(), differenceForGivenOffer.abs());
    }
}
