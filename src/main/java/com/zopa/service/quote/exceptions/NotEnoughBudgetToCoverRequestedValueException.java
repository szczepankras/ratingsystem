package com.zopa.service.quote.exceptions;

public class NotEnoughBudgetToCoverRequestedValueException extends Exception {
    private final static String ERROR_MESSAGE = "Is not possible to provide quote at this time with requested value. Please try again soon";
    public NotEnoughBudgetToCoverRequestedValueException() {
        super(ERROR_MESSAGE);
    }
}
