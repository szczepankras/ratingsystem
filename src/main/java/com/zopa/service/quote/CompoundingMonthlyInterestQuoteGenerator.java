package com.zopa.service.quote;

import com.zopa.controller.api.QuoteCalculator;
import com.zopa.model.lender.LenderOffer;
import com.zopa.model.quote.Quote;
import com.zopa.service.quote.exceptions.NotEnoughBudgetToCoverRequestedValueException;
import com.zopa.service.quote.utils.CompoundingMonthlyInterestCalculator;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;

import static com.zopa.service.quote.LendOffersExtractor.extractLenderOffersForGivenAmount;
import static com.zopa.service.quote.utils.AveragePercentageCalculator.averageRate;

@AllArgsConstructor
public class CompoundingMonthlyInterestQuoteGenerator implements QuoteCalculator {

    private static final int TIMES_OF_INTEREST_PER_YEAR = 12;

    private int numberOfMonthsForRequestedLoan;

    @Override
    public Quote calculate(Collection<LenderOffer> lenderOffers, BigDecimal requestedAmount) throws NotEnoughBudgetToCoverRequestedValueException {

        List<LenderOffer> extractedLenderOffers = extractLenderOffersForGivenAmount(lenderOffers, requestedAmount);

        double averageRate = getAverageRate(extractedLenderOffers);
        BigDecimal monthlyRepayment = getMonthlyRepayment(requestedAmount, averageRate);
        BigDecimal totalRepayment = getTotalRepayment(monthlyRepayment);
        BigDecimal averageRateRounded = new BigDecimal(convertToPercentageValue(averageRate)).setScale(1, RoundingMode.HALF_UP);

        return new Quote(requestedAmount, monthlyRepayment, totalRepayment, averageRateRounded.doubleValue());
    }

    private double getAverageRate(List<LenderOffer> extractedLenderOffers) {
        return averageRate(extractedLenderOffers);
    }

    private BigDecimal getMonthlyRepayment(BigDecimal requestedAmount, double averageRate) {
        return new CompoundingMonthlyInterestCalculator(requestedAmount, averageRate,
                TIMES_OF_INTEREST_PER_YEAR, getNumberOfYearsForMonths(numberOfMonthsForRequestedLoan))
                .compoundMonthlyInterest();
    }

    private static int getNumberOfYearsForMonths(int months){
        int result = months / 12;
        return result > 0 ? result : 1; //simplified for long term loans
    }

    private BigDecimal getTotalRepayment(BigDecimal monthlyRepayment) {
        return monthlyRepayment.multiply(new BigDecimal(numberOfMonthsForRequestedLoan));
    }

    private static double convertToPercentageValue(double value) {
        return value * 100;
    }
}
