package com.zopa.service.validation;

import com.zopa.controller.api.InputValidation;
import com.zopa.service.validation.exceptions.RequestedValueIsNotIncremental;
import com.zopa.service.validation.exceptions.RequestedValueNotInAllowedRange;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
public class InputValidator implements InputValidation {

    BigDecimal startRange;
    BigDecimal endRange;
    BigDecimal incrementValue;

    @Override
    public boolean validate(BigDecimal value) throws RequestedValueNotInAllowedRange, RequestedValueIsNotIncremental {
        if(isNotInAllowedRange(value)){
            throw new RequestedValueNotInAllowedRange(startRange, endRange);
        }
        if(isNotIncrementOf(value)){
            throw new RequestedValueIsNotIncremental(incrementValue);
        }
        return true;
    }

    public boolean isNotInAllowedRange(BigDecimal value) {
        if(value.compareTo(startRange) >= 0 && value.compareTo(endRange) <= 0){
            return false;
        }
        return true;
    }

    public boolean isNotIncrementOf(BigDecimal value) {
        return value.remainder(incrementValue).compareTo(BigDecimal.ZERO) != 0;
    }
}
