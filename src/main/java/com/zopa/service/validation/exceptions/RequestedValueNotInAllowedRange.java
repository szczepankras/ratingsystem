package com.zopa.service.validation.exceptions;


import java.math.BigDecimal;

public class RequestedValueNotInAllowedRange extends Exception {

    private static final String ERROR_MESSAGE = "Requested value is not in allowed range ";

    public RequestedValueNotInAllowedRange(BigDecimal startRange, BigDecimal endRange) {
        super(formatErrorMessage(startRange, endRange));
    }

    private static String formatErrorMessage(BigDecimal startRange, BigDecimal endRange){
        return ERROR_MESSAGE + "(" + startRange + " - " + endRange + ")";
    }
}
