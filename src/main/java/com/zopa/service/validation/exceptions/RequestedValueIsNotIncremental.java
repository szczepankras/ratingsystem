package com.zopa.service.validation.exceptions;

import java.math.BigDecimal;

public class RequestedValueIsNotIncremental extends Exception {

    private static final String ERROR_MESSAGE = "Requested value should be increment of ";

    public RequestedValueIsNotIncremental(BigDecimal increment) {
        super(ERROR_MESSAGE + increment);
    }
}
