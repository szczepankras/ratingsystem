package com.zopa;
/*
    @Author Szczepan Kras
*/
import com.zopa.controller.QuoteReportController;
import com.zopa.controller.api.InputValidation;
import com.zopa.controller.api.LenderOffersLoader;
import com.zopa.controller.api.QuoteCalculator;
import com.zopa.controller.api.QuoteOutputFormatter;
import com.zopa.service.lender.CSVLenderSortedOffersLoader;
import com.zopa.service.quote.CompoundingMonthlyInterestQuoteGenerator;
import com.zopa.service.validation.InputValidator;
import com.zopa.view.BasicQuoteOutputFormatter;

import java.math.BigDecimal;

import static com.zopa.model.constans.DataRestrictionConstans.*;

public class Application {

    private static final int NUMBER_OF_INPUTS_ARG = 2;

    public static void main(String[] args) {

        if (isValidNumberOfInputArguments(args)) {
            String fileName = extractFileName(args);
            BigDecimal requestedAmount = extractRequestedAmount(args);
            QuoteReportController quoteReportController = buildQuoteReportController(fileName);

            quoteReportController.getQuoteReportForRequestedValue(requestedAmount);

        } else {
            System.out.println("Incorrect amount of arguments. Please try with valid format: \"fileWithData.csv 1500\"");
        }
    }

    private static boolean isValidNumberOfInputArguments(String[] args) {
        return args.length == NUMBER_OF_INPUTS_ARG;
    }

    private static String extractFileName(String[] args) {
        return args[0];
    }

    private static BigDecimal extractRequestedAmount(String[] args) {
        return new BigDecimal(args[1]);
    }

    private static QuoteReportController buildQuoteReportController(String fileName) {

        QuoteCalculator quoteCalculator = new CompoundingMonthlyInterestQuoteGenerator(NUMBER_OF_MONTHS_REQUESTED);
        LenderOffersLoader lenderOffersLoader = new CSVLenderSortedOffersLoader(fileName);
        InputValidation inputValidation = new InputValidator(START_RANGE, END_RANGE, INCREMENT);
        QuoteOutputFormatter quoteOutputFormatter = new BasicQuoteOutputFormatter();

        return new QuoteReportController(
                quoteCalculator,
                lenderOffersLoader,
                inputValidation,
                quoteOutputFormatter);
    }
}
