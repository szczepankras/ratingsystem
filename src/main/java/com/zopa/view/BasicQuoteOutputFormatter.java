package com.zopa.view;

import com.zopa.controller.api.QuoteOutputFormatter;
import com.zopa.model.quote.Quote;

public class BasicQuoteOutputFormatter implements QuoteOutputFormatter {

    @Override
    public void printQuoteReport(Quote quote) {
        String report = String.format("Requested amount: £%.2f\nRate: %.1f%%\nMonthly repayment: £%.2f\nTotal repayment: £%.2f",
                quote.getLoanAmount(), quote.getRate(), quote.getMonthlyRepayment(), quote.getTotalRepayment());
        System.out.println(report);
    }

    @Override
    public void printErrorReport(String error) {
        System.out.println(error);
    }
}
