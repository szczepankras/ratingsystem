
**Business assumptions:**


- Data are sorted in order to achieve the best possible rates for clients and average rate from the best offers covering loan is calculated
- Calculation of requested loan amount is based on subtracted offset needed to cover loan

  Example:
  
  - Bob,0.075,600
  - Jane,0.16,400
  
  When requested loan is 700 we will take 600 from Bob and 100 from Jane

**Technical requirements:**

- Project build with Maven
- Used Java SDK 10, Language API Level 10
- Used "lombok" edge version (for Java 10 compatibility)
- For IntelliJ support of lombok: required "Lombok Plugin" and "Annotation Processing" need to be enabled

**Error handling:**

- Problems with file loading handled by LoadResourceException
- Not enough money on market to cover a loan handled by NotEnoughBudgetToCoverRequestedValueException
- Requested value not in a allowed range - RequestedValueNotInAllowedRange
- Requested value is not increment of 100 - RequestedValueIsNotIncremental

